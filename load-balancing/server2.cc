/*************************************************************************

Servidor número 2 - Distribución de carga uniforme sin encolar trabajos

EL servidor aquí planteado distribuye la carga de tal manera que si un 
Worker está ocupado no dejará encolar trabajos, Procsará una nueva petición
hasta que el worker cambie su estado a disponible.

Se usaron 2 colas -> La primera para los worker que están disponibles;
la segundo cola contendrá los workers ocupados hasta que envíen una respuesta.

Para ejecutar todo el sistema , solo se debe ejecutar server2 

OP = ADD - DIV -MUL - SUB


Para ejecutar el server -> server2
Para registrar un worker -> Worker1 [OP] 
Para iniciar un cliente -> client [OP] [N1] [N2] 


**************************************************************************/

#include <czmq.h>
#include <bits/stdc++.h>

using namespace std;

typedef unordered_map<string, queue<zframe_t*>> WorkerReferences;
WorkerReferences wr , wr_out;

void registerWorker(zframe_t* id, string operation) {
  zframe_print(id, "Id to add");
  zframe_t* dup = zframe_dup(id);
  zframe_print(dup, "Id copy add");
  wr[operation].push(dup);
}

zframe_t* getWorkerFor(string operation) {
  zframe_t* wid;    
  wid=wr[operation].front();   
  wr[operation].pop();   // Quitamos de la cola de disponibles
  wr_out[operation].push(wid);  // Añadimos a la cola de ocupados
  return zframe_dup(wid);   
}

void NoteWorker(zframe_t* id,char * operation){
  zframe_t* wid = zframe_dup(id); 
  wr_out[operation].front();   
  wr_out[operation].pop();   // Quitamos de la cola de No disponibles
  wr[operation].push(wid);  // Añadimos a la cola de Disponibles.
}                           

void handleClientMessage(zmsg_t* msg, void* workers , void* clients) {
  cout << "Handling the following message" << endl;
  zmsg_print(msg);
  zframe_t* clientId = zmsg_pop(msg);
  char* operation = zmsg_popstr(msg);

  if(!wr[operation].empty()){ // si en la cola existen workers para la operación 
  zframe_t* worker = getWorkerFor(operation);
  zmsg_pushstr(msg, operation);
  zmsg_prepend(msg, &clientId);
  zmsg_prepend(msg, &worker);
  zmsg_send(&msg, workers);
  cout << "End of handling" << endl;
  free(operation);
  zmsg_destroy(&msg);
  }else{ // si no , mandar mensaje de no disponibilidad.
    zmsg_t * out_msg= zmsg_new();
    zmsg_addstr(out_msg,"No hay Workers Libres para esa operacion : ");
    zmsg_addstr(out_msg,operation);     
    zmsg_prepend(out_msg,&clientId);
    zmsg_send(&out_msg, clients);
  }
}

void handleWorkerMessage(zmsg_t* msg, void* clients) {
  cout << "Handling the following WORKER" << endl;
  zmsg_print(msg);
  // Retrieve the identity and the operation code
  zframe_t* id = zmsg_pop(msg);
  char* opcode = zmsg_popstr(msg);
  if (strcmp(opcode, "register") == 0) {
    // Get the operation the worker computes
    char* operation = zmsg_popstr(msg);
    // Register the worker in the server state
    registerWorker(id, operation);
    free(operation);
  } else if (strcmp(opcode, "answer") == 0) {
    char * wr_op = zmsg_popstr(msg);
    cout<<"----------------> "<<wr_op<<endl;
    cout<<"------------Anstes---------------- "<<wr_op<<" Reintentando"<<endl;
      cout<<"tamaño de ocupados"<<wr_out[wr_op].size()<<endl;
      cout<<"tamaño de NO ocupados"<<wr[wr_op].size()<<endl;      
    NoteWorker(id,wr_op); // registramos worker al mapa de disponibles.
      cout<<"----------------------------------- "<<wr_op<<" Reintentando"<<endl;
      cout<<"tamaño de ocupados"<<wr_out[wr_op].size()<<endl;
      cout<<"tamaño de NO ocupados"<<wr[wr_op].size()<<endl;    
    zmsg_send(&msg, clients);
    

    } else {
    cout << "Unhandled message" << endl;
  }
  cout << "End of handling" << endl;
  free(opcode);
  zframe_destroy(&id);
  zmsg_destroy(&msg);
}

int main(void) {
  zctx_t* context = zctx_new();
  // Socket to talk to the workers
  void* workers = zsocket_new(context, ZMQ_ROUTER);
  int workerPort = zsocket_bind(workers, "tcp://*:5555");
  cout << "Listen to workers at: "
       << "localhost:" << workerPort << endl;

  // Socket to talk to the clients
  void* clients = zsocket_new(context, ZMQ_ROUTER);
  int clientPort = zsocket_bind(clients, "tcp://*:4444");
  cout << "Listen to clients at: "
       << "localhost:" << clientPort << endl;

  zmq_pollitem_t items[] = {{workers, 0, ZMQ_POLLIN, 0},
                            {clients, 0, ZMQ_POLLIN, 0}};
  cout << "Listening!" << endl;

  while (true) {
    zmq_poll(items, 2, 10 * ZMQ_POLL_MSEC);
    if (items[0].revents & ZMQ_POLLIN) {
      cerr << "From workers\n";
      zmsg_t* msg = zmsg_recv(workers);
      handleWorkerMessage(msg, clients);
    }
    if (items[1].revents & ZMQ_POLLIN) {
      cerr << "From clients\n";
      zmsg_t* msg = zmsg_recv(clients);   
      handleClientMessage(msg, workers , clients);
    }
  }

  // TODO: Destroy the identities

  zctx_destroy(&context);
  return 0;
}
