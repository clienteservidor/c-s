/****************************************

Servidor número 0 - Distribución de carga manera random

EL servidor aquí planteado distribuye la carga de manera aleatoria ,
así , se asegura de no escoger el mismo worker en cierto periodo de tiempo.

OP = ADD - DIV -MUL - SUB
N  = Número entero
Para iniciar un cliente -> client [OP] [N1] [N2]

Para ejecutar todo el sistema se debe ejecutar : server , luego
worker y finalmente el cliente

OP = ADD - DIV -MUL - SUB

Para ejecutar el server -> server
Para registrar un worker -> Worker [OP] 
Para iniciar un cliente -> client [OP] [N1] [N2] 

****************************************/



#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <unordered_map>
#include <czmq.h>
#include <string.h>
#include <bits/stdc++.h>

using namespace std;

//int i=0,j=0,k=0,l=0;

typedef unordered_map<string, vector<zframe_t*>> WorkerReferences;
WorkerReferences wr;


void registerWorker(zframe_t* id, string operation) {
  zframe_print(id, "Id to add");
  zframe_t* dup = zframe_dup(id);
  zframe_print(dup, "Id copy add");
  wr[operation].push_back(dup);
  cout << "Worker summary" << endl;
  for (const auto& e : wr) {
    cout << e.first << endl;
    for (zframe_t* id : e.second) {
      char* reprId = zframe_strhex(id);
      cout << reprId << " ";
      free(reprId);
    }
    cout << endl;
  }
}

zframe_t* getWorkerFor(string operation) { 
  // De manera random
  int index= rand() % wr[operation].size(); 
  zframe_t* wid = wr[operation][index];
  
  //wr[operation].front();

/* Balance uniforme beta : 
  if(operation.compare("ADD")==0){
	wid = wr[operation][i];
	i++;
	
	if(i==wr[operation].size())
		i=0;
	  }
	  
 if(operation.compare("SUB")==0){
	wid = wr[operation][j];
	j++;
	if(j==wr[operation].size())
		j=0;
	  }
  
   if(operation.compare("MUL")==0){
	wid = wr[operation][k];
	k++;
	if(k==wr[operation].size())
		k=0;
	  }
  
   if(operation.compare("DIV")==0){
	wid = wr[operation][l];
	l++;
	if(l==wr[operation].size())
		l=0;
	  }
  	  
*/
  return zframe_dup(wid);
}

void handleClientMessage(zmsg_t* msg, void* workers) {
  cout << "Handling the following message" << endl;
  zmsg_print(msg);

  zframe_t* clientId = zmsg_pop(msg);

  char* operation = zmsg_popstr(msg);
  zframe_t* worker = getWorkerFor(operation);

  zmsg_pushstr(msg, operation);
  zmsg_prepend(msg, &clientId);
  zmsg_prepend(msg, &worker);

  // Prepare and send the message to the worker
  zmsg_send(&msg, workers);

  cout << "End of handling" << endl;
  // zframe_destroy(&clientId);
  free(operation);
  zmsg_destroy(&msg);
}

void handleWorkerMessage(zmsg_t* msg, void* clients) {
  cout << "Handling the following WORKER" << endl;
  zmsg_print(msg);
  // Retrieve the identity and the operation code
  zframe_t* id = zmsg_pop(msg);
  char* opcode = zmsg_popstr(msg);
  if (strcmp(opcode, "register") == 0) {
    // Get the operation the worker computes
    char* operation = zmsg_popstr(msg);
    // Register the worker in the server state
    registerWorker(id, operation);
    free(operation);
  } else if (strcmp(opcode, "answer") == 0) {
    zmsg_send(&msg, clients);
  } else {
    cout << "Unhandled message" << endl;
  }
  cout << "End of handling" << endl;
  free(opcode);
  zframe_destroy(&id);
  zmsg_destroy(&msg);
}

int main(void) {
  zctx_t* context = zctx_new();
  // Socket to talk to the workers
  void* workers = zsocket_new(context, ZMQ_ROUTER);
  int workerPort = zsocket_bind(workers, "tcp://*:5555");
  cout << "Listen to workers at: "
       << "localhost:" << workerPort << endl;

  // Socket to talk to the clients
  void* clients = zsocket_new(context, ZMQ_ROUTER);
  int clientPort = zsocket_bind(clients, "tcp://*:4444");
  cout << "Listen to clients at: "
       << "localhost:" << clientPort << endl;

  zmq_pollitem_t items[] = {{workers, 0, ZMQ_POLLIN, 0},
                            {clients, 0, ZMQ_POLLIN, 0}};
  cout << "Listening!" << endl;

  while (true) {
    zmq_poll(items, 2, 10 * ZMQ_POLL_MSEC);
    if (items[0].revents & ZMQ_POLLIN) {
      cerr << "From workers\n";
      zmsg_t* msg = zmsg_recv(workers);
      handleWorkerMessage(msg, clients);
    }
    if (items[1].revents & ZMQ_POLLIN) {
      cerr << "From clients\n";
      zmsg_t* msg = zmsg_recv(clients);
      handleClientMessage(msg, workers);
    }
  }

  zctx_destroy(&context);
  return 0;
}
